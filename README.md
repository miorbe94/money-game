# Run project.

1. Clone project and run `npm install`.
2. copy .env.example file and rename it to .env.
3. In your new .env file set the environment variables.
4. Create a new database in postgres and execute the next query `CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`.
5. In your project execute the next command `npm run migrate`.

Now you can start the dev server with `npm run dev`.