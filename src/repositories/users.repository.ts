import { getRepository, Repository } from 'typeorm';
import { User } from '../db/entity/User';

export default class UsersRepository {

  private repository: Repository<User>;

  public constructor() {
    this.repository = getRepository(User);
  }

  getOne(id: string) {
    return this.repository.findOne(id);
  }

  getOneByEmail(email: string): Promise<User | undefined> {
    return this.repository.findOne({ where: { email } });
  }

  async getAll(): Promise<User[]> {
    return this.repository.find();
  }

  async create(user: User): Promise<User> {
    const userToCompare = await this.getOneByEmail(user.email);
    if (userToCompare) throw new Error('Email is already in use');
    return await this.repository.save(user);
  }
}