require('dotenv').config();

import { App } from './app';
import config from './config';
import {createConnection} from "typeorm";

async function main() {
  try {
    await createConnection();
    console.log('DB working');
    const app = new App(+config.port);
    app.listen();
  } catch (error) {
    console.log(error);
  }
}

main();