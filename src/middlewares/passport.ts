import { Strategy, ExtractJwt, StrategyOptions } from 'passport-jwt';
import config from '../config';
import { container } from '../container';
import { asValue } from 'awilix';
import UsersService from '../services/users.service';

const opts: StrategyOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.jwtSecret,
};

export default new Strategy(opts, async (payload, done) => {
  const usersService: UsersService = container.resolve('usersService');
  const user = await usersService.getUser(payload.id);
  if (user) {
    container.register({
      user: asValue(user),
    });
    return done(null, user);
  }
  return done(null, false);
});