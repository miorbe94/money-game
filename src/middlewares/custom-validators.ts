import { Request, Response } from "express";
import { validationResult } from "express-validator";

export const validatorError = (req: Request, res: Response, next: Function) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  next();
}

export const passwordConfirmation = (value: string, { req }: any) => {
  if (value !== req.body.password) {
    throw new Error('Password confirmation does not match password');
  }
  return true;
}
