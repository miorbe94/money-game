import { json } from 'express';
import morgan from 'morgan';
import cors from 'cors';
import passport from 'passport';
import authMiddleware from '../middlewares/passport';

export default [
  morgan('dev'),
  cors(),
  json(),
  passport.use(authMiddleware).initialize(),
];
