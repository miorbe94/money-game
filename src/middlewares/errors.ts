import { Request, Response } from "express";

export default (error: Error, req: Request, res: Response, next: Function) => {
  return res.status(500).json(error.message)
}
