export default interface IConfig {
  port: string | number;
  jwtSecret: string;
}