import IConfig from './iconfig';

const config: IConfig = {
  port: process.env.PORT || 4000,
  jwtSecret: process.env.JWT_SECRET || 'secret-key',
}

export default config;
