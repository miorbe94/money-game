import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert } from 'typeorm';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import config from '../../config';

@Entity()
export class User {

  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @Column()
  name!: string;

  @Column()
  email!: string;

  @Column()
  password!: string;

  @BeforeInsert()
  async encryptPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }

  constructor(name: string, email: string, password: string) {
    this.name = name;
    this.email = email;
    this.password = password;
  }

  getDTO() {
    return {
      id: this.id,
      name: this.name,
      email: this.email,
    };
  }

  static getDTOs(users: User[]) {
    return users.map(user => user.getDTO());
  }

  createToken() {
    return jwt.sign({ id: this.id }, config.jwtSecret, { expiresIn: '1h' });
  }

  comparePassword(password: string) {
    return bcrypt.compare(password, this.password);
  }

}
