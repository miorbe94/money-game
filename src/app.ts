import express, { Express } from 'express';
import container from './container';
import { loadControllers } from 'awilix-express';
import middlewares from './middlewares';
import errorHandler from './middlewares/errors';

export class App {

  private app: Express = express();

  constructor(private port: number | string = 4000) {
    this.middlewares();
  }

  private middlewares(): void {
    this.app.use(middlewares);
    this.app.use(container, loadControllers('controllers/*', { cwd: __dirname }));
    this.app.use(errorHandler);
  }

  async listen() {
    await this.app.listen(this.port);
    console.log('Server on port', this.port);
  }
}