import { route, GET, before } from 'awilix-express'
import { Request, Response } from 'express';
import UsersService from '../services/users.service';
import passport from 'passport';
import { User } from '../db/entity/User';
 
@route('/users')
export default class UserAPI {

  private usersService: UsersService;

  constructor({ usersService }: any) {
    this.usersService = usersService;
  }
 
  @GET()
  @before(passport.authenticate('jwt', { session: false }))
  async getUsers(req: Request, res: Response) {
    const users = await this.usersService.getAllUsers();
    return res.json(User.getDTOs(users));
  }

}
