import { route, POST, before } from 'awilix-express'
import { Request, Response } from 'express';
import SessionService from '../services/session.service';
import { body } from 'express-validator';
import { validatorError, passwordConfirmation } from '../middlewares/custom-validators';
import UsersService from '../services/users.service';
import { User } from '../db/entity/User';


@route('/session')
export default class UserAPI {

  private sessionService: SessionService;
  private usersService: UsersService;

  constructor({ sessionService, usersService }: any) {
    this.sessionService = sessionService;
    this.usersService = usersService;
  }

  @POST()
  @route('/signin')
  @before([
    body('email')
      .notEmpty().withMessage('Email is required')
      .isEmail().withMessage('Email must be a valied email'),
    body('password')
      .notEmpty().withMessage('Password is required')
      .isLength({ min: 6 }).withMessage('Password must be at least 6 characters'),
    validatorError
  ])
  async login(req: Request, res: Response) {
    const token = await this.sessionService.login(req.body.email, req.body.password);
    return res.json({ token });
  }

  @POST()
  @route('/signup')
  @before([
    body('name').notEmpty().withMessage('Name is required'),
    body('email')
      .notEmpty().withMessage('Email is required')
      .isEmail().withMessage('Email is not a valid email'),
    body('password')
      .notEmpty().withMessage('Password is required')
      .isLength({ min: 6 }).withMessage('Password must be at least 6 characters'),
    body('password_confirmation').custom(passwordConfirmation),
    validatorError,
  ])
  async createUser(req: Request, res: Response) {
    const { name, email, password } = req.body;
    const user = await this.usersService.createUser(new User(name, email, password));
    return res.json(user.getDTO());
  }
  
}
