import { createContainer, InjectionMode, Lifetime } from 'awilix';
const { scopePerRequest } = require('awilix-express')

const container = createContainer()
container.loadModules(['src/services/*', 'src/repositories/*'], {
  formatName: 'camelCase',
  resolverOptions: {
    injectionMode: InjectionMode.PROXY,
    lifetime: Lifetime.SINGLETON,
  }
})

export {container};
export default scopePerRequest(container);
