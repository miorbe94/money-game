import UsersRepository from "../repositories/users.repository";
import { User } from '../db/entity/User';

export default class UsersService {

  private usersRepository: UsersRepository

  constructor({ usersRepository }: any) {
    this.usersRepository = usersRepository;
  }

  getUser(id: string): Promise<User | undefined> {
    return this.usersRepository.getOne(id);
  }

  getUserByEmail(email: string): Promise<User | undefined>  {
    return this.usersRepository.getOneByEmail(email);
  }

  getAllUsers(): Promise<User[]> {
    return this.usersRepository.getAll();
  }

  createUser(user: User): Promise<User> {
    return this.usersRepository.create(user);
  }

}