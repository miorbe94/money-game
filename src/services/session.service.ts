import UsersService from './users.service';

export default class SessionService {

  private usersService: UsersService

  constructor({ usersService }: any) {
    this.usersService = usersService;
  }

  async login(email: string, password: string) {
    const user = await this.usersService.getUserByEmail(email);
    if (!user) throw new Error('User does not exist');

    const passwordMatches = await user.comparePassword(password);
    if (!passwordMatches) throw new Error('The email and password you entered did not match');

    return user.createToken();
  }
}
